
CPPHFLAGS = -g -std=c++14
CPPNWFLAGS = $(CPPHFLAGS) -I "lib" -I"obj" -I"src" -fmax-errors=1
CPPFLAGS = $(CPPNWFLAGS) -Wall

.PHONY: all

all: bin/netCompil

.PHONY: clean

clean:
	rm -rf bin
	rm -rf obj
	rm -f test/test.cpp
	rm -f test/test

.PHONY: mrproper

mrproper: clean
	rm -f conv/conv
	rm -f lib/boost.hpp.gch


.PHONY: install
install: all
	mv bin/netCompil /usr/bin/netCompil

.PHONY:uninstall
uninstall:
	rm /usr/bin/netCompil

lib/boost.hpp.gch : lib/boost.hpp
	g++ $< $(CPPHFLAGS)

bin/netCompil : obj/main.o obj/lex.o obj/yacc.o obj/AST.o obj/Ordo.o
	@mkdir -p bin
	g++ $^ -o $@ $(CPPFLAGS) -lboost_program_options

obj/lex.cpp : src/lex.ll
	@mkdir -p obj
	{ echo "#include \"boost.hpp\" " & flex -t $< ;} > $@

obj/lex.o : obj/lex.cpp src/Scanner.h obj/yacc.cpp lib/boost.hpp.gch
	@mkdir -p obj
	g++ -c $(CPPNWFLAGS) $< -o $@ -g -I "lib" -I"obj" -I"src"

obj/yacc.cpp : src/net.yy
	@mkdir -p obj
	bison src/net.yy -d -o obj/yacc.cpp

obj/yacc.o : obj/yacc.cpp src/AST.h src/Scanner.h lib/boost.hpp.gch
	@mkdir -p obj
	g++ -c $< -o $@ $(CPPFLAGS)

conv/conv: conv/converter.cpp
	g++ $< -o $@ $(CPPFLAGS)

obj/generic.inc : src/generic.cpp conv/conv
	@mkdir -p obj
	cat $< | ./conv/conv > $@

obj/main.o : src/main.cpp src/AST.h src/Ordo.h obj/generic.inc src/Scanner.h obj/yacc.cpp lib/boost.hpp.gch
	@mkdir -p obj
	g++ -c $< -o $@ $(CPPFLAGS)

obj/AST.o : src/AST.cpp src/Scanner.h src/AST.h obj/yacc.cpp lib/boost.hpp.gch
	@mkdir -p obj
	g++ -c $< -o $@ $(CPPFLAGS)

obj/Ordo.o : src/Ordo.cpp
	@mkdir -p obj
	g++ -c $< -o $@ $(CPPFLAGS)

