  #include<iostream>
  #include"yacc.hpp"
  #include"Scanner.h"
  
  using namespace std;
  using namespace yy;
  typedef yy::parser::token token;
  
  

  #define yyerror error

%option c++
%option yyclass="Scanner"
%option yylineno
%option noyywrap

  // This code is executed at each action : update position in code.
  #define YY_USER_ACTION loc->step(); loc-> columns(yyleng);

  

%%
\n     { loc->lines(); if(inok) return '\n';};
,      { return ',';};
=   { return '=';};
:    { return ':';};
INPUT  { return token::INPUT;};
OUTPUT { return token::OUTPUT;};
VAR    { return token::VAR;};
AND { return token::AND;};
CONCAT { return token::CONCAT;};
MUX { return token::MUX;};
NAND { return token::NAND;};
NOT { return token::NOT;};
OR { return token::OR;};
RAM { return token::RAM;};
REG { return token::REG;};
ROM { return token::ROM;};
SELECT { return token::SELECT;};
SLICE { return token::SLICE;};
XOR { return token::XOR;};
 
IN     { inok = true; return token::IN;};

[0-9]+	{ yylval->build(atoi (yytext));;return token::CST;}

[_a-z](_|[A-Z]|[a-z]|[0-9])*      {yylval->build(std::string(yytext)); return token::IDENT;}  ; 
 
[\t ]+          /* ignore */ ;   

. {  error(*loc,"unknown caracter " + string(yytext)); exit(EXIT_FAILURE); 
}
  

%%



