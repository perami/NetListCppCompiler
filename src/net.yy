%code requires{
	#include <string>
	#include <vector>
	#include"AST.h"
	class Scanner;

}


%{
  #include"boost.hpp"
  #include <iostream>
  #include"AST.h"
  #include"Scanner.h"


  using namespace std;

  void error (const yy::location& loc,const std::string& st)
  {
 	//cout << "hey" << endl;
 	if(loc.begin.line != loc.end.line)
   	  cerr <<"File \"" << *loc.begin.filename << "\" , line " << loc.begin.line << "-" << loc.end.line << " : " << st<< endl;
   	else
   	  cerr <<"File \"" << *loc.begin.filename << "\" , line " << loc.begin.line << ", characters " << loc.begin.column << "-" << loc.end.column <<": " << st<< endl;
    exit(EXIT_FAILURE);
  }
  void yy::parser::error(const yy::location& loc,const std::string& st)
  {
	::error(loc,st); // calling global error function and not this one.
  }


%}


// C++ decalration
%skeleton "lalr1.cc"
%language "c++"
%define api.value.type variant


//list of tokens
%token '\n'
%token ','
%token '='
%token ':'
%token INPUT
%token OUTPUT
%token VAR
%token IN
%token <std::string>IDENT
%token <int> CST
%token AND CONCAT MUX NAND NOT OR RAM REG ROM SELECT SLICE XOR


//type  declaration of non-terminal rules
%type <std::vector<std::string>> varlist
%type <std::vector<Statement*> > text
%type <Statement*> stat
%type <Statement*> expr // bizarre mais opérationel
%type <Operator> binop
%type <Var> var

//take the scanner(lexer) as a param)
%parse-param {Scanner& scan}
%parse-param {ProgramNet& res}

%code{
// declare the parser fonction to call :
#define yylex scan.yylex
}

// Better error explanation than "syntax error"
%define parse.error verbose

//Location tracking for errors
%locations

//Location itnitialisation
%initial-action
{
  @$.initialize (scan.getName());
};



%%


program :
INPUT varlist OUTPUT varlist VAR vartlist IN text
{
	res.input = move($2);
	res.output = move($4);
	res.code = move($8);
}
  ;

varlist :
	      {$$ = std::vector<std::string>();}
    |
    IDENT {$$ = std::vector<std::string>{move($1)};}
	|
	varlist ',' IDENT {$1.push_back($3); $$ = $1; }
;

vartlist :
    tVar
	|
	vartlist ',' tVar
;

tVar:
	IDENT
{
	if(Types.find($1) != Types.end())
		error(@1,"The variable \"" + $1 + "\" is already declared");
	Types[$1]=1;
}
	|
	IDENT ':' CST
{
	if(Types.find($1) != Types.end())
		error(@1,"The variable \"" + $1 + "\" is already declared");
	if ($3 <=0)
	{
		error(@$,"zero or negative number of wire on var \""+ $1 + "\"");

	}
	if($3 > 64)
	{
		error(@$,"The current implementation does not allow arrays wider than 64 bits (var : "+ $1 + ")");
	}
	Types[$1] = $3;
}

text:
	stat
{
	 if($1)
	 	$$ = vector<Statement*> {$1};
}
	|
	text '\n' stat
{
	if($3)
		$1.push_back($3); $$ = $1;
}
;

stat:
		{$$ = nullptr;}
	|
	IDENT '=' expr
{
	if(Types.find($1) == Types.end())
		error(@1,"\"" + $1 +"\" was not declared");
	$3->dest = move($1);
	$3->loc = @3;
	$$ = $3;
}
;

expr :
	var {$$ = new Affect($1);}
	|
	REG var {$$ = new Reg($2);  }
	|
	NOT var {$$ = new Not($2);  }
	|
	binop var var {$$ = new BinOp($1,$2,$3); }
	|
	MUX var var var {$$ = new Mux($2,$3,$4);}
	|
	ROM CST CST var {$$ = new Rom($2,$3,$4);}
	|
	RAM CST CST var var var var {$$ = new Ram($2,$3,$4,$5,$6,$7);}
	|
	CONCAT var var	{$$ = new Concat($2,$3);}
	|
	SELECT CST var 	{$$ = new Select($2,$3);}
	|
	SLICE CST CST var	{$$ = new Slice($2,$3,$4);}
;

var:
	IDENT
	{
		if (scan.inok && Types.find($1) == Types.end())
			error(@1,"\""+ $1 + "\" was not declared");
		$$ = Var($1);}
	|
	CST {$$ = Var($1);}
;



binop:
	AND {$$ = Operator::AND;}
	|
	OR {$$ = Operator::OR;}
	|
	XOR {$$ = Operator::XOR;}
	|
	NAND {$$ = Operator::NAND;}
;




