#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

void runWindow();

int main(int argc, char* argv[])
{
    static_assert(sizeof(lint) == 8, "The typedef lint is not 8 bytes");
    array<uchar,shortSize> evens{}, odds{};
    array<lint,longSize> evenl{}, oddl{};
    string romfile ;
    uchar *ROM = nullptr,*RAM = nullptr;
    lint nb = 0,end = 0;
    for(int i =1 ; i < argc ; ++i)
    {
        string s = argv[i];
        if (s == "--rom")
        {
            if(i+1 >= argc)
            {
                cerr<< "option --rom is not followed by a file name";
                return 1;
            }
            romfile = argv[i+1];
            ++i;
        }
        if(s == "--ram")
        {
            if(i+1 >= argc)
            {
                cerr<< "option --ram is not followed by a number";
                return 1;
            }
            ramsize =atoll(argv[i+1]);
            i++;
        }
        if(s == "-n")
        {
            if(i+1 >= argc)
            {
                cerr<< "option -n is not followed by a number";
                return 1;
            }
            end =atoll(argv[i+1]);
            i++;
        }
    }
    if (romfile != "")
    {
        ifstream romst (romfile);
        if(!romst)
        {
            cerr << "unable to open " << romfile << " for ROM." << endl;
            return 1;
        }
        romst.seekg (0, romst.end);
        romsize = romst.tellg();
        romst.seekg(0,romst.beg);
        try
        {
            ROM = new uchar[romsize+32];
        }
        catch (const std::bad_alloc& b)
        {
            cerr << "ROM of size " << romsize << " : allocation failed" <<endl;
            return EXIT_FAILURE;
        }
        romst.read(reinterpret_cast<char*>(ROM),romsize);
        romsize +=24;
    }
    if(ramsize > 0)
    {

        try
        {
            RAM = new uchar[ramsize+8](); // () for zero initialisation
        }
        catch (const std::bad_alloc& b)
        {
            cerr << "RAM of size " << ramsize << " : allocation failed" <<endl;
            return EXIT_FAILURE;
        }
    }

    cout << "Circuit starting with ROM : " << romfile << " and "
         << ramsize << " bytes of RAM";
    if(end) cout <<", for " << end << " cycles";
    cout<<"." << endl;

    sf::Thread thread(&runWindow);
    thread.launch();

    while(!end || nb < end) // while no Ctrl-C
    {
        if(nb % 2 == 0)
            sim(odds,evens,oddl,evenl,ROM,RAM);
        else
            sim(evens,odds,evenl,oddl,ROM,RAM);
        nb++;
    }
    thread.terminate();
}

lint read(bool isRam,const uchar* mem,lint beg, lint size) // size must be 64 or below 56
{
    lint memsize = isRam ? ramsize : romsize;
    lint mask = (1L << size) -1L;
    if( size == 8 or size == 16 or size == 32 or size == 64)
    {
        if(beg*(size/8) < 0 or beg*(size/8) >= memsize){
            cerr << "Out of bound on read on " << (isRam ? "RAM" : "ROM")
                 << " of size " << memsize << " with index " << beg*(size/8);
            exit(1);
        }
        return *(reinterpret_cast<const lint*>(mem+beg*(size/8))) & mask;
    }
    else if(size <= 56)
    {
        if( (beg*size)/8< 0 or (beg*size)/8 >= memsize){
            cerr << "Out of bound on read on " << (isRam ? "RAM" : "ROM")
                 << " of size " << memsize << " with index " << (beg*size)/8;
            exit(1);
        }
        return *(reinterpret_cast<const lint*>(mem+(beg*size)/8))>>((beg* size) % 8) & mask;
    }
    else
    {
        cerr << "ROM/RAM size above 56 but not 64 is not supported" <<endl;
        exit(1);
    }
}

void write(bool isRam,uchar* mem,int beg, lint size,lint data) // size must 64 or below 56
{
    lint memsize = isRam ? ramsize : romsize;
    lint mask = (1L << size) -1L;
    if( size == 8 or size == 16 or size == 32 or size == 64)
    {
        if(beg*(size/8) < 0 or beg*(size/8) >= memsize){
            cerr << "Out of bound on write on" << (isRam ? "RAM" : "ROM")
                 << "of size " << memsize << " with index " << beg*(size/8);
            exit(1);
        }
        lint& val = *(reinterpret_cast<lint*>(mem+beg*(size/8)));
        val &= (~mask);
        val |= (data & mask);
    }
    else if(size <= 56)
    {
        if( (beg*size)/8 < 0 or (beg*size)/8 >= memsize){
            cerr << "Out of bound on write on" << (isRam ? "RAM" : "ROM")
                 << "of size " << memsize << " with index " << (beg*size)/8;
            exit(1);
        }
        mask = mask <<((beg *size) % 8);
        data = data <<((beg * size) % 8);
        lint& val = *(reinterpret_cast<lint*>(mem+(beg*size)/8));
        val &= (~mask);
        val |= (data & mask);
    }
    else
    {
        cerr << "ROM/RAM size above 56 but not 64 is not supported" <<endl;
        exit(1);
    }
}

enum {VOID,SEC,MIN,HOUR,WD,DAY,MONTH,YEAR};

int gpuVars[8];

char clockString[100] = {0};
sf::Mutex mutex;

void print_gpu(){
    static int n = 0;
    if(n == 100*1000) {
        mutex.lock();
        snprintf(clockString, 100, "%0.2dh:%0.2dm:%0.2ds  %0.2d/%0.2d/%d", gpuVars[HOUR], gpuVars[MIN], gpuVars[SEC], gpuVars[DAY], gpuVars[MONTH], gpuVars[YEAR]);
        mutex.unlock();
        n = 0;
    }
    ++n;
}

void runWindow() {
    sf::RenderWindow window(sf::VideoMode(800, 300), "Mega Clock (tm)");
    window.setFramerateLimit(25);

    sf::Font font;
    if (!font.loadFromFile("DejaVuSansMono.ttf")) {
        fprintf(stderr, "CAN'T OPEN FONT\n");
        exit(-1);
    }
    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(50);

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }
        mutex.lock();
        std::string str = clockString;
        mutex.unlock();
        text.setString(str);
        window.clear(sf::Color::Black);
        window.draw(text);
        window.display();
    }
    exit(0);
}

void mappings(uchar * RAM){
    int error = *(int *) RAM;
    if (error){
        cerr << "Error Code : " << error << endl;
        exit(0);
    }
    int t = time(nullptr);
    *(int*)(RAM +8) = t;
    int gpu = *(int*)(RAM +4);
    if (gpu){
        int code = gpu & 3;
        if (code == 2){
            int instr = (gpu & 0b11111100) >> 2;
            if (instr >= SEC and instr <= YEAR){
                int val = gpu >> 16;
                gpuVars[instr] = val;
            }
            *(int*)(RAM +4) = 0;
        }
    }
    print_gpu();
}

