#pragma once

#include<vector>
#include<list>
#include<set>


enum class State{ NotVisited, InProgress, Visited};


struct Node
{
    State sta;
    int id;
    std::set<int> depOn;
    std::set<int> depOnOpt;
    std::set<int> neededFor;
    std::set<int>neededForOpt;
};

/*struct Block
{
    Block(Node & node) : depOn(node.depOn), depOnOpt(node.depOnOpt),
        neededForOpt(node.neededForOpt), neededForOpt(node.neededForOpt) {}
    State sta;
    int id;
    std::set<Node*>content;
    std::set<int> depOn;
    std::set<int> depOnOpt;
    std::set<int> neededFor;
    std::set<int>neededForOpt;

    }*/



struct Graph
{
    std::vector<Node> nodes;
    //std::set<Blocks>
    Graph(int n) : nodes (n){};
    void addNode();
    void addEdge(int res,int dep);

    template<typename Iter>
        void addEdges(int res,Iter beg, Iter end);

    void clearMarks();
    std::list<int> findRoots();
    bool hasCycle();
    std::list<int> topological();

};


