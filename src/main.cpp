#include"boost.hpp"

#include<iostream>
#include<map>
#include<fstream>
#include<sstream>
#include <exception>

#include"prettyprint.hpp"

#include "yacc.hpp"
#include "AST.h"
#include "Ordo.h"
#include "Scanner.h"

using namespace std;
using namespace boost;
using namespace boost::program_options;





string readVar(string name )
{
  stringstream ss;
  int size = Types[name].toInt();
  ss <<"  cout << \"" << name << "\" << \" ? \" <<endl;" << endl;
  ss <<"  cin >> _tmp_ ;" << endl;
  ss <<"  " << fromString(name) << " = _tmp_ &" << ((1L<<Types[name].toInt()) -1L ) <<";" << endl;
  if(size > 1) ss << "  cout << bitset<" <<size << ">(" << fromString(name) << ") << endl;";

  return ss.str();
}
string printVar(string name)
{
  stringstream ss;
  int size = Types[name].toInt();
  long long int mask = (size == 64)? 0L : ~((1L << size)-1L);
  ss <<"  cout << \"" << name << "\" << \" : \" << lint(" << fromString(name) << "& " << ~mask << ") ;" << endl;
  if(size > 1) ss <<"  if (" << fromString(name) << " >> " << size-1 << ") cout << \" / \" << (long long int)(" << mask << "L | lint(" << fromString(name) <<"));" <<endl;
  if(size > 1) ss << "  cout << \" / \" << bitset<" << size << ">(" << fromString(name) << ");";
  ss << "  cout << endl;"<<endl;
  return ss.str();
}


int main(int argc, char** argv)
{
  string outFileName;
  string inFileName;
  istream * input= nullptr;

  positional_options_description p;
  p.add("in", -1);

  options_description desc("Options");
  desc.add_options()
    ("help,h", "Produce help message")
    ("out,o", value<string>(&outFileName)->default_value("a.cpp"), "Output file")
	("in,i", value<string>(), "Input file (just arg is also valid)");

  variables_map vm;
  try
	{
	store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
	notify(vm);
	}
  catch (const std::exception &e)
	{
	  cerr << e.what() <<endl;
	  return EXIT_FAILURE;
	}

  if (vm.count("help"))
	{
	  cout << desc << "\n";
	  return EXIT_SUCCESS;
	}
  if(vm.count("in"))
	{
	  inFileName = vm["in"].as<string>();
	  input = new ifstream(inFileName);
	  if(input->fail())
		{
		  cerr << "Error : Couldn't open " << inFileName << endl;
		  return EXIT_FAILURE;
		}
	}
  else
	{
	  cerr << "Error : No Input file" << endl;
	  return EXIT_FAILURE;
	}




  //--------------PARSING----------------------
  Scanner scan(input,inFileName);
  ProgramNet res;
  yy::parser parser(scan,res);
  parser.parse();
  // check if input variables are declared
  for( auto name : res.input)
	{
	  if(Types.find(name) == Types.end())
		{
		  cerr << "File \""<< inFileName <<"\" :" << endl;
		  cerr <<" The input var \"" << name << "\" was not declared in VAR"  << endl;
		  return EXIT_FAILURE;
		}
	}
  // check if output variables are declared
  for( auto name : res.output)
	{
	  if(Types.find(name) == Types.end())
		{
		  cerr << "File \""<< inFileName  <<"\" :" << endl;
		  cerr <<" The output var \"" << name << "\" was not declared in VAR"  << endl;
		  return EXIT_FAILURE;
		}
	}
  //check if there are not two statement that assign to the same variable

  for (auto st1 : res.code)
      for(auto st2 : res.code){
          if(st1 != st2 && st1->dest == st2-> dest){
              cerr << "File \""<< st1->loc.begin.filename << "\" :" << endl;
              cerr << "The instruction at lines " << st1->loc.begin.line << " and "
                   << st2->loc.begin.line << " define the same variable : " << st1->dest << endl;
          }
      }

  cout << "This netlist contains "<<res.code.size() << " instructions" <<endl;



  // --------------TYPE check---------------------------
  for(auto st : res. code)
	{
	  st->typeCheck(); // vérifie que st est correctement typé, appelle exit(1) en cas de problème
	}
  cout << "Typing is OK, Compilation begin" << endl;

  // ordonancing
  Graph g (res.code.size());
  map<std::string,int> nameToNum; // inverse of res.code
  for(unsigned int i = 0 ; i < res.code.size();++i)
	{
	  nameToNum[res.code[i]->dest] = i;
	}
  for(auto sta : res. code)
	{
	  int id = nameToNum[sta->dest];
	  for(auto dep : sta->depend())
		{
		  match(dep,
				[](int b){},
				[id,&nameToNum,&g](string s)
				{
				  auto it = nameToNum.find(s);
				  if( it != nameToNum.end())
					{
					  g.addEdge(id,it->second);
					}
				});
		}
	}
  if(g.hasCycle())
	{
	  cerr << "File \""<<inFileName <<
		"\" :\n There is a combinatory loop : unable to compile" << endl;
	  return EXIT_FAILURE;
	}
  auto tmp = g.topological();
  std::vector<Statement*> ordSt;
  for(auto i : tmp)
	{
	  ordSt.push_back(res.code[i]);
	}





  // writing
  ofstream outfile (outFileName);
  //initial code (includes)
  outfile << "#include<iostream>" << endl;
  outfile << "#include<array>" << endl;
  outfile << "#include<fstream>" << endl;
  outfile << "#include<bitset>" << endl;
  outfile << "#include<ctime>" << endl;
  outfile << "using namespace std;" << endl<<endl;
  outfile << "typedef unsigned long long int lint;" << endl<<endl;
  outfile << "typedef unsigned char uchar;" << endl<<endl;
  outfile << "lint romsize = 0, ramsize = 0; // in Bytes"<<endl <<endl;

  //var enumerations
  outfile << "enum shorts{";
  for(auto s : Types)
	{
	  if(s.second <= 8)
		outfile << s.first << " , ";
	}
  outfile << "SHORT_LAST};" << endl;
  outfile << "const int shortSize = SHORT_LAST;" << endl<<endl;

  outfile << "enum longs{";
  for(auto s : Types)
	{
	  if(s.second > 8)
		outfile << s.first << " , ";
	}
  outfile << "LONG_LAST};" << endl;
  outfile << "const int longSize = LONG_LAST;" << endl<<endl;

  //----------------PROTOTYPE DE SIM-------------
  outfile << "void sim (const array<uchar,shortSize>& olds, "
		  <<"array<uchar,shortSize>& news, "
		  <<"const array<lint,longSize>& oldl, "
		  <<"array<lint,longSize>& newl, "
		  <<"const uchar * ROM, "
		  <<"uchar *RAM);"
		  << endl;

  //------------------------MAIN------------------
#include"generic.inc" //external genrated file (conv)

  //------------------------SIM------------------
  outfile << "void sim (const array<uchar,shortSize>& olds, "
		  <<"array<uchar,shortSize>& news, "
		  <<"const array<lint,longSize>& oldl, "
		  <<"array<lint,longSize>& newl, "
		  <<"const uchar * ROM, "
		  <<"uchar *RAM)"
		  << endl;
  outfile << "{"  <<endl;

  //lecture :
  outfile << "\tlint _tmp_ = 0;"<<endl;
  for(auto s : res.input)
	{
	  outfile << readVar(s);
	}
  outfile <<endl;
  //écriture du code : new[VAR] = new[var1] & new[VAR2];
  for (auto sta : ordSt)
	{
	  outfile << "  " <<  sta->toCpp()<<endl;
	}
  outfile <<endl;
  //affichage des résultats
  for(auto s : res.output)
	{
	  outfile << printVar(s);
	}

  outfile <<endl;

  for(auto sta : ordSt)
	{
	  outfile << sta-> writeRAM();
	}

  int ramprsize = 0;
  for(int i =0 ; i < ramprsize ; ++i){
      outfile << "  if(RAM != nullptr) cout << bitset<8>((lint)RAM[ " << i
              << "]) <<" <<(((i+1)%8)? "\" \"" : "\"\\n\"") << ";" << endl;
              }
  outfile << "  if(RAM) mappings(RAM);" << endl;

  outfile << "}" << endl<<endl;

  cout << "Compilation to " << outFileName << " is done" << endl;

  return 0;


}
