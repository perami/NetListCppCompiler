#include "boost.hpp"


#include "Ordo.h"
#include<functional>
#include <stdexcept>
#include<iostream>
#include"prettyprint.hpp"

using namespace std;

void Graph::addNode()
{
  nodes.push_back({State::NotVisited,{},{}});
}
void Graph::addEdge(int res,int dep)
{
  nodes.at(res).depOn.insert(dep);
  nodes.at(dep).neededFor.insert(res);
}
template<typename Iter>
void addEdges(int res,Iter beg, Iter end)
{
    for(Iter it =beg ; it != end ; ++it){
        addEdge(res,*it);
    }
}

void Graph::clearMarks()
{
  for(Node & n : nodes)
    {
      n.sta = State::NotVisited;
    }
}
std::list<int> Graph::findRoots()
{
  list<int> res;
  for(unsigned int i =0 ; i < nodes.size() ;++i)
    {
      if(nodes.at(i).depOn.empty())
        res.push_back(i);
    }
  return res;
}




bool Graph::hasCycle()
{
    clearMarks();
    auto l = findRoots();
    if(l.empty())
        return true;

    function<void(Node&)> exp = [this,&exp] (Node& n){
        switch (n.sta) {
        case State::NotVisited:
        n.sta = State::InProgress;
        for (int i : n.neededFor)
            exp(nodes.at(i));
        n.sta = State::Visited;
        break;

        case State::InProgress:
        throw 0;
        break;
        case State::Visited :
        break;
        }
    };

    try{
        for(int i : l)
            exp(nodes.at(i));
        //check that all have been explored
        for(auto n : nodes)
            if(n.sta != State::Visited)
                return true;
        // if we arrive here, all is OK
        return false;

    }catch (...){
        return true;
    }
}


std::list<int> Graph::topological()
{
  if(hasCycle())
	throw runtime_error("The Graph has cycles : topological fails");
  clearMarks();
  auto l = findRoots();
  list<int> res;

  function<void(int)> exp = [this,&exp,&res] (int id)
	{
	  //cout << "exploring " << id << " "<<res<<endl;
	  Node& n = nodes.at(id);
	  switch (n.sta)
		{
    case State::NotVisited:
        n.sta = State::Visited;
        for (int i : n.neededFor)
            exp(i);
        res.push_front(id);
        break;

    case State::InProgress:
        cerr << "topological has found a cycle" << endl;
        exit(1);
        break;
    case State::Visited :
        break;
		}
	};

  for(int i : l)
	exp(i);
  return res;
}
