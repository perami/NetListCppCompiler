#include"boost.hpp"
#include "AST.h"
#include "Scanner.h"
#include <sstream>

using namespace std;



map<string,Type> Types;

typedef unsigned long long int lint;


Type getType(const std::string& v)
{
  return Types[v];
}
Type getType(Var v){
    return match(v,
                 [](int i){return Type();},
                 static_cast<Type(*)(const std::string&)>( getType));
}


/// To print the variable name for debbuging or error messages
std::string toName(const Var& v)
{
  return match(v,
			   [](int i)-> std::string { return "Constant";},
			   [](const std::string& s){return s;});
}



// ---------------------------------TYPE CHECKING-------------------------------------

void Affect::typeCheck()
{
  if(getType(dest) != getType(var))
	  typeerror("Direct Affectation types don't match");
}

void Not::typeCheck()
{
  if(getType(dest) != getType(var))
	  typeerror("Negation types don't match");
}
void Reg::typeCheck()
{
  if(getType(dest) != getType(var))
	{
	  typeerror("Register types don't match");
	}
}

void BinOp::typeCheck()
{
  //std::cout << dest << " " << var1 << " " << var2 << std::endl;
  if(getType(dest) != getType(var1) or getType(dest) != getType(var2) )
	{
	  typeerror("Binary Operator types don't match");
	}
}
void Mux::typeCheck()
  {
	if (getType(select) > 1)
	  {
		typeerror("Muxer selector "+ toName(select) + " is not a single wire");
	  }
	if(getType(dest) != getType(var1) or getType(dest) != getType(var2) )
	  {
		typeerror("Muxer operands types don't match");
	  }
  }
void Rom::typeCheck()
{
  if(getType(dest) != word)
	typeerror("ROM word size does not match destination type");
  if(getType(ra) != addr)
	typeerror("ROM addresse size does not match the type of " + toName(ra));
}
void Ram::typeCheck()
{
  if(getType(dest) != word)
	typeerror("RAM output word size does not match destination type");
  if(getType(ra) != addr)
	typeerror("RAM read address size does not match the type of " + toName(ra));
  if(getType(we) > 1)
	typeerror("RAM write enabling bit \"" + toName(we) + "\" isn't a single wire");
  if(getType(wa) != addr)
	typeerror("RAM write address size does not match the type of " + toName(wa));
  if(getType(data) != word)
	typeerror("RAM input word size does not match the type of " + toName(data));
}
void Concat::typeCheck()
{
    if(getType(dest).toInt() != getType(var1).toInt() + getType(var2).toInt())
	{
	  typeerror("Concat operands types don't match");
	}
}
void Select::typeCheck()
{
  if(getType(dest) > 1)
	{
	  typeerror("Destination type must be a wire in SELECT");
	}
  if(i < 0 or getType(var) <= i)
	{
	  typeerror("Index out of bound in SELECT");
	}

}
void Slice::typeCheck()
{
  if(max < min)
	{
	  typeerror("The second integer is lower than the first in SLICE");
	}
  if(getType(dest) != max - min + 1)
	{
	  typeerror("Destination type is wrong sized in SLICE");
	}
  if( getType(var) <= max)
	typeerror("The upper bound is too large in SLICE");
  if( min <0 or min > max )
	typeerror("The lower bound is negative or too large in SLICE");
}

void Statement::typeerror (const std::string&  msg)
{
  error(loc,msg);
}





//------From Variable to the text in code----------------------------

string fromInt(int i)
{
    stringstream ss;
    ss << i;
    return ss.str();
}

string fromString(string s,bool old) // old is for register
{
  stringstream ss;
  if(old)
	ss << "old";
  else
	ss<<"new";

  if(getType(s) <= 8)
	ss << "s[" << s <<"]";
  else
	ss << "l[" << s <<"]";
  return ss.str();
}

string fromVar(Var v, bool old)
{
  using namespace std::placeholders;// for using _1
  return match(v,
			   fromInt,
			   function<string(string)>(bind(fromString,_1,old)));
}

//---------------------------------COMPILATION----------------------------------------
string Affect::toCpp()
{
  stringstream ss;
  ss << fromString(dest) << " = " << fromVar(var) << ";";
  return ss.str();
}

string Not::toCpp()
{
    lint mask = ((lint)(1) << getType(var).toInt()) -1;
  stringstream ss;
  ss << fromString(dest) << " = ~(" << fromVar(var) << ") & " << mask <<" ;"; 
  return ss.str();
}
string Reg::toCpp()
{
  stringstream ss;
  ss << fromString(dest) << " = " << fromVar(var,true) << ";";
  return ss.str();
}
string BinOp::toCpp()
{
  stringstream ss;

  ss << fromString(dest) << " = ";
  if(op == Operator::NAND)
	ss << "~";

  ss << "( " << fromVar(var1) << " ";
  switch(op)
	{
	  case Operator::AND:
	  case Operator::NAND:
		ss << "&";
		break;
	  case Operator::OR:
		ss << "|";
		break;
	  case Operator::XOR:
		ss << "^";
		break;
	}
  ss << " " <<fromVar(var2) << " )";
  if(op == Operator::NAND)
	{
      lint mask = ((lint)(1) << getType(dest).toInt()) -1;
	  ss << "&" << mask;
	}
  ss <<";";
  return ss.str();
}
string Mux::toCpp()
{
  stringstream ss;
  ss << fromString(dest) << " = ("
	 << fromVar(select) << " & 1L) ? "
	 << fromVar(var1) << " : "
	 << fromVar(var2) << ";";
  return ss.str();
}
string Rom::toCpp()
{
  stringstream ss;
  ss << fromString(dest) << " = "
	 << "read(false,ROM," << fromVar(ra) <<"," << word <<");";
  return ss.str();
}
string Ram::toCpp()
{
  stringstream ss;
  ss << fromString(dest) << " = "
	 << "read(true,RAM," << fromVar(ra) <<"," << word <<");";
  return ss.str();
}
string Concat::toCpp()
{
  stringstream ss;
  int decal = getType(var1).toInt();
  ss << fromString(dest) << " = ("
	 << fromVar(var2) << " << " << decal
	 << ") + (( (lint(1) << "<<decal<<") -1)& " << fromVar(var1) << ");";
  return ss.str();
}
string Select::toCpp()
{
  lint mask = (lint)(1) << i;
  stringstream ss;
  ss << fromString(dest) << " = ("
	 << fromVar(var) << " & " << mask << ") >> "
	 << i <<";";
  return ss.str();
}
string Slice::toCpp()
{
    lint mask = ((1L << (max+1))-1) & (~(min > 0 ? ((1L << (min -1)) -1) : 0));
  //cout << mask;
  stringstream ss;
  ss << fromString(dest) << " = ("
	 << fromVar(var) << " & " << mask << ") >> "
	 << min <<";";
  return ss.str();
}


//--------------------------------WRITE RAM--------------------------------------

std::string Ram::writeRAM()
{
  stringstream ss;
  ss<<"  if("<<fromVar(we) <<")"
	<< "write(true,RAM," << fromVar(wa) << "," << word << "," << fromVar(data) << ");" <<endl;
  return ss.str();
}
