#pragma once



#include<vector>
#include<set>
#include<string>
#include<stdexcept>

#include<map>
#include<iostream>


typedef boost::variant<int,std::string> Var;

class Type {
    int val;
public :
    Type() : val(-1) {}
    Type(int i) : val(i) {}
    bool operator == (Type oth){
        if(val == -1) return true;
        if(oth.val == -1) return true;
        return val == oth.val;
    }
    bool operator != (Type oth) {
        return !(oth == *this);
    }
    bool operator == (int i){
        if (val == -1) return true;
        return val == i;
    }
    bool operator > (int i){
        if (val == -1) return true;
        return val > i;
    }
    bool operator <= (int i){
        if (val == -1) return true;
        return val <= i;
    }
    int toInt(){
        if (val == -1) throw std::runtime_error("Convert poly type to integer");
        return val;
    }
};

extern std::map<std::string,Type> Types;


Type getType(const std::string& v);
Type getType(Var v);
std::string fromString(std::string s,bool old = false); // old is for register
std::string fromVar(Var v, bool old = false);
#include "location.hh"

struct Statement
{
  std::string dest;
  virtual void typeCheck() = 0;
  virtual std::vector<Var> depend() = 0;
  virtual std::string toCpp()= 0;
  virtual std::string writeRAM(){return "";}
  yy::location loc;
protected:
  void typeerror (const std::string&  msg);
};

struct ProgramNet
{
public:

  std::vector<std::string> input;
  std::vector<std::string> output;
  std::vector<Statement*> code;
};

struct Affect : public Statement
{
  Affect(Var nvar) : var(nvar){}
  Var var;
  void typeCheck();
  std::vector<Var> depend(){return {var};}
  std::string toCpp();
};

struct Not : public Statement
{
  Not(Var nvar) : var(nvar){}
  Var var;
  void typeCheck();
  std::vector<Var> depend(){return {var};}
  std::string toCpp();
};
struct Reg : public Statement
{
  Reg(Var nvar) : var(nvar){}
  Var var;
  void typeCheck();
  std::vector<Var> depend(){return {};}
  std::string toCpp();

};


enum class Operator
{
  AND,OR,NAND,XOR
	};

struct BinOp : public Statement
{
  BinOp(Operator nop,Var nvar1,Var nvar2) :op(nop),var1(nvar1), var2(nvar2){}
  Operator op;
  Var var1;
  Var var2;
  void typeCheck();
  std::vector<Var> depend(){return {var1,var2};}
  std::string toCpp();
};

struct Mux : public Statement
{
  Mux(Var nsel,Var nvar1,Var nvar2) :select(nsel),var1(nvar1), var2(nvar2){}
  Var select;
  Var var1;
  Var var2;
  void typeCheck();
  std::vector<Var> depend(){return {select,var1,var2};}
  std::string toCpp();
};
struct Rom : public Statement
{
  Rom(int naddr, int nword,Var nra) :addr(naddr),word(nword), ra(nra){}
  int addr;
  int word;
  Var ra;
  void typeCheck();
  std::vector<Var> depend(){return {ra};}
  std::string toCpp();
};
struct Ram : public Statement
{
  Ram(int naddr, int nword,Var nra,Var nwe,Var nwa,Var ndata) :
	addr(naddr),word(nword), ra(nra),wa(nwa),we(nwe),data(ndata){}
  int addr;
  int word;
  Var ra;
  Var wa;
  Var we;
  Var data;
  void typeCheck();
  std::vector<Var> depend(){return{ra};}
  std::string toCpp();
  std::string writeRAM();
};
struct Concat : public Statement
{
  Concat(Var nvar1,Var nvar2) :var1(nvar1), var2(nvar2){}

  Var var1;
  Var var2;
  void typeCheck();
  std::vector<Var> depend(){return {var1,var2};}
  std::string toCpp();

};
struct Select : public Statement
{
  Select(int ni,Var nvar) :i(ni),var(nvar){}
  int i;
  Var var;
  void typeCheck();
  std::vector<Var> depend(){return {var};}
  std::string toCpp();

};
struct Slice : public Statement
{
  Slice(int nmin,int nmax,Var nvar):min(nmin),max(nmax),var(nvar){}
  int min;
  int max;
  Var var;
  void typeCheck();
  std::vector<Var> depend(){return {var};}
  std::string toCpp();

};


